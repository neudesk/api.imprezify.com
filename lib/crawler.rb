require 'capybara'
require 'capybara/dsl'

module Crawler
  class LinkedIn
    include Capybara::DSL
    def initialize
      Capybara.default_driver = :selenium
      Capybara.run_server = false
      Capybara.app_host = 'https://www.linkedin.com'
      SkillCategory.all.delete_all
      SkillTag.all.delete_all
    end

    def getSkillCategory()
      visit "/directory/topics/"
      sleep 5
      click_link 'Sign in'
      fill_in 'login-email', Settings.crawler.linkedin['username']
      fill_in 'login-password', with: Settings.crawler.linkedin['password']
      click_button 'login-submit'
      sleep 1
      ('a'..'z').each do |alpha|
        visit "/directory/topics-#{alpha}/"
        page.all(:css, "body#pagekey-seo_topics_directory.js[dir='ltr'] div#application-body main#layout-main[role='main'] div#seo-dir.ktf-dir div.wrapper > .section.last").each do |section|
          unless section['class'].include? 'standalone-buckets'
            category_hrefs = []
            categories = section.all(:css, ".content > a")
            categories.each_with_index do |category, category_idx|
              puts "saving category #{category.text} with href #{category['href']}"
              skill_category = SkillCategory.create(name: category.text)
              category_hrefs << {href: category['href'], category: skill_category}
            end

            category_hrefs.each do |href|
              visit href[:href]
              sleep 1
              page.all(:css, "body#pagekey-seo_topics_directory.js[dir='ltr'] div#application-body main#layout-main[role='main'] div#seo-dir.ktf-dir div.wrapper .content > a").each do |tree|
                puts "saving skill #{tree.text}"
                href[:category].skill_tags.create(name: tree.text)
              end
            end

          end
        end
      end
    end

  end
end

# this should be updates