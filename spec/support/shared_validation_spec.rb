require 'rails_helper'

RSpec.shared_examples 'has_validation_error' do
  it 'should not be valid' do
    expect(model.valid?).to be false
  end

  it 'should validate error message' do
    model.valid?
    expect(model.errors[field]).to include(message)
  end

end