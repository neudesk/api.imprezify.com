FactoryGirl.define do
  factory :char_reference do
    name Faker::Name.name
    title Faker::Company.profession
    contact_number Faker::PhoneNumber.phone_number
  end
end
