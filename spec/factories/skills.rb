FactoryGirl.define do
  factory :skill do
    name Faker::Superhero.power
    rating (1..5).to_a.sample
  end
end
