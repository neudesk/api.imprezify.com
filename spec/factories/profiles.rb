FactoryGirl.define do
  factory :profile do
    overview Faker::Lorem.words(10).join(' ')
    title Faker::Company.profession
    birthday (Time.now - 30.years)
    address Faker::Address.street_address
    photo File.new(Rails.root.join('spec/fixtures/passport-photo-size-uk.jpg'))
  end
end
