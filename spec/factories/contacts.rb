FactoryGirl.define do
  factory :contact do
    name %W(email skype website facebook linkedin phone mobile).sample
    value Faker::Lorem.word
  end
end
