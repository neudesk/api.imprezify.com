FactoryGirl.define do
  factory :education do
    school Faker::Educator.university
    degree Faker::Educator.campus
    field_of_study Faker::Educator.course
    activities Faker::Lorem.words(10).join(' ')
    from_year (Time.now - 5.year).year
    to_year (Time.now - 1.year).year
    description Faker::Lorem.words(10).join(' ')
    average_grade 'A'
  end
end
