FactoryGirl.define do
  factory :language do
    name Dictionaries::Languages.map {|x| x[:name]}.sample
  end
end
