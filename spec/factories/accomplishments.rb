FactoryGirl.define do
  factory :accomplishment do
    accomplishment_type Faker::Superhero.power
    title Faker::Superhero.name
    description Faker::Lorem.words(10).join(' ')
    from_month Time.now.year - 1
    from_year Time.now.month - 2
    to_month Time.now.year
    to_year Time.now.month
  end
end
