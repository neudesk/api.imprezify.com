FactoryGirl.define do
  factory :experience do
    title Faker::Superhero.name
    company Faker::Company.name
    location Faker::Address.full_address
    from_month 12
    from_year (Time.now - 1.year).year
    to_month 6
    to_year Time.now.year
  end
end
