require 'rails_helper'

RSpec.describe Experience, type: :model do

  %W(title company location from_month from_year profile).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:experience, {profile: profile, column.to_sym => nil} ) }
      let(:message) { column == 'profile' ? 'must exist' : "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

  [{name: 'to_month', value: 14}, {name: 'to_year', value: (Time.now - 61.years).year}].each do |column|
    context "validate inclusion of #{column} only if present" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:experience, {profile: profile, column[:name].to_sym => column[:value]} ) }
      let(:message) { "is not included in the list" }
      let(:field) { column[:name].to_sym }
      include_examples 'has_validation_error'
    end
  end

end
