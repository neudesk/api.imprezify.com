require 'rails_helper'

RSpec.describe Language, type: :model do

  context 'name' do
    let(:user) { create(:user) }
    let(:profile) { create(:profile, user: user) }
    let(:model) { build(:language, {profile: profile, name: nil} ) }
    it 'should validate presence of name' do
      model.valid?
      expect(model.errors[:name]).to include "can't be blank"
    end

    it 'should accept valid language only' do
      model.name = Faker::Superhero.power
      model.valid?
      expect(model.errors[:name]).to include 'is not included in the list'
    end

  end

end
