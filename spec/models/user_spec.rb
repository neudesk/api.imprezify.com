require 'rails_helper'

RSpec.describe User, type: :model do

  context 'roles' do
    let(:admin) { create(:user, role: 4) }
    let(:support) { create(:user, role: 3) }
    let(:company_admin) { create(:user, role: 2) }
    let(:individual) { create(:user, role: 1) }

    it 'should be admin' do
      expect(admin.admin?).to eq true
    end

    it 'should be support' do
      expect(support.support?).to eq true
    end

    it 'should be company_admin?' do
      expect(company_admin.company_admin?).to eq true
    end

    it 'should be individual' do
      expect(individual.individual?).to eq true
    end

  end

end
