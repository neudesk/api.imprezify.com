require 'rails_helper'

RSpec.describe Skill, type: :model do

  %W(name rating).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:skill, {profile: profile, column.to_sym => nil} ) }
      let(:message) { "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

  context 'rating' do
    let(:user) { create(:user) }
    let(:profile) { create(:profile, user: user) }
    let(:model) { build(:skill, {profile: profile, rating: 11} ) }
    it 'should validate rating values' do
      model.valid?
      expect(model.errors[:rating]).to include 'should be greater than 0 and lesser than 5'
    end
  end

end
