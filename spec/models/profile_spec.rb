require 'rails_helper'

RSpec.describe Profile, type: :model do

  %W(overview title birthday address).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:model) { build(:profile, {user: user, column.to_sym => nil} ) }
      let(:message) { "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

  context 'photo' do
    let(:user) { create(:user) }
    let(:model) { create(:profile, user: user) }
    it 'should has passport size' do
      expect(model.photo.url(:passport).present?).to be true
      expect(model)
    end
  end

end
