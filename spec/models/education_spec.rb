require 'rails_helper'

RSpec.describe Education, type: :model do
  %W(school degree field_of_study from_year).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:education, {profile: profile, column.to_sym => nil} ) }
      let(:message) { column == 'profile' ? 'must exist' : "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

  context 'validates presence of to_year if current is false' do
    let(:user) { create(:user) }
    let(:profile) { create(:profile, user: user) }
    let(:model) { build(:education, {profile: profile, to_year: nil, current: true} ) }
    it 'should says no error' do
      expect(model.valid?).to be true
    end
  end

end
