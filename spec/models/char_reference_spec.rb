require 'rails_helper'

RSpec.describe CharReference, type: :model do

  %W(name title contact_number).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:char_reference, {profile: profile, column.to_sym => nil} ) }
      let(:message) { column == 'profile' ? 'must exist' : "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

end
