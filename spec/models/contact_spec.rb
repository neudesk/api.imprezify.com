require 'rails_helper'

RSpec.describe Contact, type: :model do

  %W(name value).each do |column|
    context "validate presence of #{column}" do
      let(:user) { create(:user) }
      let(:profile) { create(:profile, user: user) }
      let(:model) { build(:contact, {profile: profile, column.to_sym => nil} ) }
      let(:message) { column == 'profile' ? 'must exist' : "can't be blank" }
      let(:field) { column.to_sym }
      include_examples 'has_validation_error'
    end
  end

  context 'validate value of icon' do
    let(:user) { create(:user) }
    let(:profile) { create(:profile, user: user) }
    let(:list) { Contact::LIST }
    let(:name) { list.map {|x| x[:name]}.sample }
    let(:model) { create(:contact, {profile: profile, name: name} ) }
    let(:ico) { list.select {|x| x[:name] == name}.first[:icon] }
    it 'should get exect value of icon' do
      expect(model.icon).to eq("fa #{ico}")
    end
  end

  context 'validate acceptable name' do
    let(:user) { create(:user) }
    let(:profile) { create(:profile, user: user) }
    let(:name) { Faker::Superhero.power }
    let(:model) { build(:contact, {profile: profile, name: name} ) }

    it 'should raise not on the list error' do
      model.valid?
      expect(model.errors[:name].first).to eq('is not included in the list')
    end
  end

end
