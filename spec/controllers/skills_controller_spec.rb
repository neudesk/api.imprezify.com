require 'rails_helper'

RSpec.describe SkillsController, type: :controller do

  context 'index' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:skills) {(0..1).map {|x| create(:skill, profile: profile)}}
    it 'should get skills' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be skills.length
    end
  end

  context 'create' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let(:name) {Faker::Superhero.power}
    it 'should able to create skill' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: {skill: {name: name, rating: 4.5}}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['name']).to eq name
    end

    let!(:skill) {build(:skill, profile: profile)}
    let(:params) {{skill: {name: Faker::Superhero.power, rating: 11}}}
    it 'should accept valid value only for rating' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: params
      expect(response).to have_http_status :unprocessable_entity
    end

  end

  context 'update' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:skill) {create(:skill, profile: profile)}
    let!(:new_name) {Faker::Superhero.power}
    it 'should be able to update skill' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: {id: skill.id, skill: {name: new_name}}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['name']).to eq new_name
    end
  end

  context 'destroy' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:skill) {create(:skill, profile: profile)}
    it 'should able to delete skill' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: skill.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
