require 'rails_helper'

RSpec.describe LanguagesController, type: :controller do

  context 'index' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:languages) { (0..1).map { |x| create(:language, profile: profile) } }
    it 'should get languages' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be languages.length
    end
  end

  context 'create' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let(:name) { Dictionaries::Languages.map {|x| x[:name]}.sample }
    it 'should able to create language' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: { language: { name: name } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['name']).to eq name
    end

    let!(:language) { build(:language, profile: profile) }
    let(:params) { { language: { name: Faker::Superhero.power } } }
    it 'should accept valid value only' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: params
      expect(response).to have_http_status :unprocessable_entity
    end

  end

  context 'update' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:language) { create(:language, profile: profile) }
    let!(:new_name) { Dictionaries::Languages.map {|x| x[:name]}.sample }
    it 'should be able to update language' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: { id: language.id, language: { name: new_name } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['name']).to eq new_name
    end
  end

  context 'destroy' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:language) { create(:language, profile: profile) }
    it 'should able to delete language' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: language.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
