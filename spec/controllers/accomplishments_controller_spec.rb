require 'rails_helper'

RSpec.describe AccomplishmentsController, type: :controller do

  context 'index' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:accomplishments) {(0..1).map {|x| create(:accomplishment, profile: profile)}}
    it 'should get accomplishments' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be accomplishments.length
    end
  end

  context 'create' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let(:accomplishment) {create(:accomplishment, profile: profile)}

    let(:params) {
      {
          accomplishment: {accomplishment_type: accomplishment.accomplishment_type,
                           title: accomplishment.title,
                           description: accomplishment.description,
                           from_month: accomplishment.from_month,
                           from_year: accomplishment.to_year,
                           to_month: accomplishment.to_month,
                           to_year: accomplishment.to_year}
      }
    }

    it 'should able to create accomplishment' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: params
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map {|v, k| k}.length).to be > 0
    end

    let!(:accomplishment) {build(:accomplishment, profile: profile)}

    %W(accomplishment_type title description from_month from_year to_month to_year).each do |column|
      it "should require #{column}" do
        request.headers.merge!(user.create_new_auth_token)
        params[:accomplishment].delete(column.to_sym)
        post :create, params: params
        expect(response).to have_http_status :unprocessable_entity
      end
    end

  end

  context 'update' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:accomplishment) {create(:accomplishment, profile: profile)}
    let!(:new_title) {Faker::Company.profession}
    it 'should be able to update accomplishment' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: {id: accomplishment.id,
                            accomplishment: {
                                title: new_title
                            }}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['title']).to eq new_title
    end
  end

  context 'destroy' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:accomplishment) {create(:accomplishment, profile: profile)}
    it 'should able to delete accomplishment' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: accomplishment.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
