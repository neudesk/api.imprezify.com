require 'rails_helper'

RSpec.describe ExperiencesController, type: :controller do

  context 'index' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:experiences) { (0..1).map { |x| create(:experience, profile: profile) } }
    it 'should get experiences' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be experiences.length
    end
  end

  context 'create' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    it 'should able to create experience' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: { experience: { title: Faker::Company.profession,
                                             company: Faker::Company.name,
                                             location: Faker::Address.full_address,
                                             from_month: Time.now.month,
                                             from_year: (Time.now - 1.year).year,
                                             to_month: (Time.now - 1.month).month,
                                             to_year: Time.now.year } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map{|v,k| k}.length).to be > 0
    end

    let!(:experience) { build(:experience, profile: profile) }
    let(:params) {
      {
          experience: { title: Faker::Company.profession,
                        company: Faker::Company.name,
                        location: Faker::Address.full_address,
                        from_month: Time.now.month,
                        from_year: (Time.now - 1.year).year,
                        to_month: (Time.now - 1.month).month,
                        to_year: Time.now.year }
      }
    }
    %W(title company location from_month from_year).each do |column|
      it "should require #{column}" do
        request.headers.merge!(user.create_new_auth_token)
        params[:experience].delete(column.to_sym)
        post :create, params: params
        expect(response).to have_http_status :unprocessable_entity
      end
    end

  end

  context 'update' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:experience) { create(:experience, profile: profile) }
    let!(:new_title) { Faker::Company.profession }
    it 'should be able to update experience' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: { id: experience.id,
                             experience: {
                                 title: new_title
                             } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['title']).to eq new_title
    end
  end

  context 'destroy' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:experience) { create(:experience, profile: profile) }
    it 'should able to delete experience' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: experience.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
