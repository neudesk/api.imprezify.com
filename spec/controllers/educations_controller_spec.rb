require 'rails_helper'

RSpec.describe EducationsController, type: :controller do

  context 'index' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:educations) {(0..1).map {|x| create(:education, profile: profile)}}
    it 'should get educations' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be educations.length
    end
  end

  context 'create' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let(:education) {build(:education)}
    it 'should able to create education' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: {education: {school: education.school,
                                         degree: education.degree,
                                         field_of_study: education.field_of_study,
                                         activities: education.activities,
                                         from_year: education.from_year,
                                         to_year: education.to_year,
                                         description: education.description,
                                         average_grade: 'A'}}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map {|v, k| k}.length).to be > 0
    end

    let!(:education) {build(:education, profile: profile)}
    let(:params) {
      {
          education: {school: education.school,
                      degree: education.degree,
                      field_of_study: education.field_of_study,
                      activities: education.activities,
                      from_year: education.from_year,
                      to_year: education.to_year,
                      description: education.description,
                      average_grade: 'A'}
      }
    }
    %W(school degree field_of_study from_year).each do |column|
      it "should require #{column}" do
        request.headers.merge!(user.create_new_auth_token)
        params[:education].delete(column.to_sym)
        post :create, params: params
        expect(response).to have_http_status :unprocessable_entity
      end
    end

  end

  context 'update' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:education) {create(:education, profile: profile)}
    let!(:new_average_grade) { 'A+' }
    it 'should be able to update education' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: {id: education.id,
                            education: {average_grade: new_average_grade}}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['average_grade']).to eq new_average_grade
    end
  end

  context 'destroy' do
    let!(:user) {create(:user)}
    let!(:profile) {create(:profile, user: user)}
    let!(:education) {create(:education, profile: profile)}
    it 'should able to delete education' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: education.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
