require 'rails_helper'

RSpec.describe CharReferencesController, type: :controller do

  context 'index' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:char_references) { (0..1).map { |x| create(:char_reference, profile: profile) } }
    it 'should get char_references' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be char_references.length
    end
  end

  context 'create' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let(:reference) { build(:char_reference) }
    it 'should able to create char_reference' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: { char_reference: { name: reference.name, title: reference.title, contact_number: reference.contact_number } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map{|v,k| k}.length).to be > 0
    end

    let!(:char_reference) { build(:char_reference, profile: profile) }
    let(:params) { { char_reference: { name: reference.name, title: reference.title, contact_number: reference.contact_number } } }
    %W(name title contact_number).each do |column|
      it "should require #{column}" do
        request.headers.merge!(user.create_new_auth_token)
        params[:char_reference].delete(column.to_sym)
        post :create, params: params
        expect(response).to have_http_status :unprocessable_entity
      end
    end

  end

  context 'update' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:char_reference) { create(:char_reference, profile: profile) }
    let!(:new_title) { Faker::Company.profession }
    it 'should be able to update char_reference' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: { id: char_reference.id, char_reference: { title: new_title } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['title']).to eq new_title
    end
  end

  context 'destroy' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:char_reference) { create(:char_reference, profile: profile) }
    it 'should able to delete char_reference' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: char_reference.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
