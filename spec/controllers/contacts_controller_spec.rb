require 'rails_helper'

RSpec.describe ContactsController, type: :controller do

  context 'index' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:contact) { (0..1).map { |x| create(:contact, profile: profile) } }
    it 'should get contacts' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).length).to be contact.length
    end
  end

  context 'create' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    it 'should able to create contact' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: { contact: { name: Contact::LIST.map {|x| x[:name]}.sample,
                                         value: Faker::Superhero.power }
      }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map{|v,k|  k}.length).to be > 0
    end

    let!(:contact) { build(:contact, profile: profile) }
    let(:params) {
      {
          contact: { name: Contact::LIST.map {|x| x[:name]}.sample,
                     value: Faker::Superhero.power }
      }
    }
    %W(name value).each do |column|
      it "should require #{column}" do
        request.headers.merge!(user.create_new_auth_token)
        params[:contact].delete(column.to_sym)
        post :create, params: params
        expect(response).to have_http_status :unprocessable_entity
      end
    end

  end

  context 'update' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:contact) { create(:contact, profile: profile) }
    let!(:new_name) { Contact::LIST.map {|x| x[:name]}.sample }
    it 'should be able to update contact' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: { id: contact.id,
                             contact: { name: new_name } }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['name']).to eq new_name
    end
  end

  context 'destroy' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let!(:contact) { create(:contact, profile: profile) }
    it 'should able to delete contact' do
      request.headers.merge!(user.create_new_auth_token)
      delete :destroy, params: {id: contact.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['success']).to eq true
    end
  end

end
