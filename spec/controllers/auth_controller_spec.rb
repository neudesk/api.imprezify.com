require 'rails_helper'

RSpec.describe 'AuthController', type: :request do

  let(:email) { Faker::Internet.email }
  let!(:user) {  FactoryGirl.create(:user) }

  context 'authentication' do

    it 'should able to signup' do
      post '/auth', params: { email: email,
                              password: 'ABC12abc',
                              password_confirmation: 'ABC12abc' }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['data']['email']).to eq(email)
    end

    it 'should able to login and get token' do
      post '/auth/sign_in', params: { email: user.email, password: 'ABC12abc' }
      expect(response).to have_http_status(200)
      expect(response.headers['access-token'].present?).to be true
    end

  end

end
