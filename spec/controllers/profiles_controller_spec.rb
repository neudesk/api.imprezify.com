require 'rails_helper'

RSpec.describe ProfilesController, type: :controller do

  context 'index' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    it 'should response with 200' do
      request.headers.merge!(user.create_new_auth_token)
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['overview']).to eq(profile.overview)
    end
  end

  context 'create' do
    let!(:user) { create(:user) }
    let(:overview) { Faker::Lorem.words(10).join(' ') }
    it 'should response with 200' do
      request.headers.merge!(user.create_new_auth_token)
      post :create, params: {
        profile: { overview: overview, title: Faker::Company.profession, birthday: Time.now - 30.years, address: Faker::Address.full_address }
      }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['overview']).to eq(overview)
    end
  end

  context 'update' do
    let!(:user) { create(:user) }
    let!(:profile) { create(:profile, user: user) }
    let(:overview) { Faker::Lorem.words(10).join(' ') }
    it 'should response with 200' do
      request.headers.merge!(user.create_new_auth_token)
      put :update, params: {
        id: profile.id,
        profile: { overview: overview }
      }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['overview']).to eq(overview)
    end
  end

end
