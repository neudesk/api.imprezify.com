class CreateContacts < ActiveRecord::Migration[5.1]
  def up
    create_table :contacts do |t|
      t.string :name
      t.string :value
      t.string :icon
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :contacts
  end

end
