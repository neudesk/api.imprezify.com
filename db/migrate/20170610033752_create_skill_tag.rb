class CreateSkillTag < ActiveRecord::Migration[5.1]
  def up
    create_table :skill_tags do |t|
      t.integer :skill_category_id
      t.string :name
      t.timestamps
    end
  end

  def down
    drop_table :skill_tags
  end

end
