class CreateSkills < ActiveRecord::Migration[5.1]
  def up
    create_table :skills do |t|
      t.string :name
      t.decimal :rating, precision: 2, scale: 1
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :skills
  end

end
