class CreateCharReferences < ActiveRecord::Migration[5.1]
  def up
    create_table :char_references do |t|
      t.string :name
      t.string :title
      t.string :contact_number
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :char_references
  end
end
