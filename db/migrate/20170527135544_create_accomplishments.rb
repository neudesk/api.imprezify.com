class CreateAccomplishments < ActiveRecord::Migration[5.1]
  def up
    create_table :accomplishments do |t|
      t.string :accomplishment_type
      t.string :title
      t.text :description
      t.integer :from_month
      t.integer :from_year
      t.integer :to_month
      t.integer :to_year
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :accomplishments
  end

end
