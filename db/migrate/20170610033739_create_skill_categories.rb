class CreateSkillCategories < ActiveRecord::Migration[5.1]
  def up
    create_table :skill_categories do |t|
      t.string :name
      t.timestamps
    end
  end

  def down
    drop_table :skill_categories
  end

end
