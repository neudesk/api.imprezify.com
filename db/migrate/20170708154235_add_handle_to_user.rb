class AddHandleToUser < ActiveRecord::Migration[5.1]
  def up
    add_column :users, :handle, :string
  end

  def down
    remove_column :users, :handle
  end
end
