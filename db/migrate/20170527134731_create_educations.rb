class CreateEducations < ActiveRecord::Migration[5.1]
  def up
    create_table :educations do |t|
      t.string :school
      t.string :degree
      t.string :field_of_study
      t.text :activities
      t.integer :from_year
      t.integer :to_year
      t.boolean :current, default: false
      t.text :description
      t.string :average_grade
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :educations
  end

end
