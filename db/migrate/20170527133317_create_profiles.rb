class CreateProfiles < ActiveRecord::Migration[5.1]
  def up
    create_table :profiles do |t|
      t.text :overview
      t.string :title
      t.integer :user_id
      t.string :address
      t.datetime :birthday
      t.timestamps
    end

    add_attachment :profiles, :photo
  end

  def down
    drop_table :profiles
    remove_attachment :profiles, :photo
  end
end
