class CreateExperiences < ActiveRecord::Migration[5.1]
  def up
    create_table :experiences do |t|
      t.string :title
      t.string :company
      t.string :location
      t.integer :from_month
      t.integer :from_year
      t.integer :to_month
      t.integer :to_year
      t.boolean :current, default: false
      t.text :description
      t.integer :profile_id
      t.timestamps
    end
  end

  def down
    drop_table :experiences
  end

end
