Rails.application.routes.draw do
  apipie
  mount_devise_token_auth_for 'User', at: 'auth'

  resources :profiles do
  end

  resources :experiences do
  end

  resources :contacts
  resources :educations
  resources :accomplishments
  resources :languages
  resources :skills
  resources :char_references
  resources :users

end
