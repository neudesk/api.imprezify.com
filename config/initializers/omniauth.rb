Rails.application.config.middleware.use OmniAuth::Builder do
  provider :linkedin,
           Settings.omniauth[:linkedin][:key],
           Settings.omniauth[:linkedin][:secret]
end
