class LanguagesController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'languages', 'current user languages'
  def index
    render json: current_user.profile.languages
  end

  api :POST, 'languages', 'create user languages'
  param :language, Hash, :desc => 'create user languages', required: true do
    param :name, Dictionaries::Languages.map {|x| x[:name]}, required: true
  end
  def create
    render json: current_user.profile.languages.create!(valid_params)
  end

  api :PATCH, 'languages/:id', 'update user languages'
  param :id, String, required: true
  param :language, Hash, :desc => 'update user languages', required: true do
    param :name, Dictionaries::Languages.map {|x| x[:name]}, required: false
  end
  def update
    language = current_user.profile.languages.find(params[:id])
    language.update_attributes!(valid_params) unless language.blank?
    render json: language
  end

  api :DELETE, 'languages/:id', 'delete user language'
  param :id, String, required: true
  def destroy
    destroyed = nil
    language = current_user.profile.languages.find(params[:id])
    destroyed = language.destroy unless language.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:language).permit!
  end

end
