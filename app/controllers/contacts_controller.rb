class ContactsController < ApplicationController

  api :GET, 'contacts', 'current user contacts'
  def index
    render json: current_user.profile.contacts
  end

  api :POST, 'contacts', 'create user contacts'
  param :contact, Hash, :desc => 'create user contacts', required: true do
    param :name, String, required: true
    param :value, String, required: true
  end
  def create
    render json: current_user.profile.contacts.create!(valid_params)
  end

  api :PATCH, 'contacts/:id', 'update user contacts'
  param :id, String, required: true
  param :contact, Hash, :desc => 'create user contacts', required: true do
    param :name, String, required: false
    param :name, String, required: false
  end
  def update
    contact = current_user.profile.contacts.find(params[:id])
    contact.update_attributes!(valid_params) unless contact.blank?
    render json: contact
  end

  api :DELETE, 'contacts/:id', 'delete user contacts'
  param :id, String, required: true
  def destroy
    destroyed = nil
    contact = current_user.profile.contacts.find(params[:id])
    destroyed = contact.destroy unless contact.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:contact).permit!
  end

end
