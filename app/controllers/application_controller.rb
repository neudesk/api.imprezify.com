class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ActiveRecord::RecordInvalid, with: :invalid_record
  rescue_from Apipie::ParamMissing, with: :missing_params
  rescue_from Apipie::ParamInvalid, with: :missing_params

  private

  def missing_params(exception)
    render json: { error: exception.message, stacktrace: exception.inspect }, status: :unprocessable_entity
  end

  def record_not_found(exception)
    render :json => {:error => exception.message, stacktrace: exception.inspect}, :status => :not_found
  end

  def invalid_record(exception)
    render json: {error: exception.record.errors, stacktrace: exception.inspect}, status: :unprocessable_entity
  end

end
