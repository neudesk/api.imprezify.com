class AccomplishmentsController < ApplicationController

  api :GET, 'accomplishments', 'current user accomplishments'
  def index
    render json: current_user.profile.accomplishments
  end

  api :POST, 'accomplishments', 'create user accomplishments'
  param :accomplishment, Hash, :desc => 'create user accomplishment', required: true do
    param :accomplishment_type, String, required: true
    param :title, String, required: true
    param :description, String, required: true
    param :from_month, String, required: true
    param :from_year, String, required: true
    param :to_month, String, required: true
    param :to_year, String, required: true
  end
  def create
    render json: current_user.profile.accomplishments.create!(valid_params)
  end

  api :PATCH, 'accomplishments/:id', 'update user accomplishments'
  param :id, String, required: true
  param :accomplishment, Hash, :desc => 'create user accomplishment', required: true do
    param :accomplishment_type, String, required: false
    param :title, String, required: false
    param :description, String, required: false
    param :from_month, String, required: false
    param :from_year, String, required: false
    param :to_month, String, required: false
    param :to_year, String, required: false
  end
  def update
    accomplishment = current_user.profile.accomplishments.find(params[:id])
    accomplishment.update_attributes!(valid_params)
    render json: accomplishment
  end

  api :DELETE, 'accomplishments/:id', 'delete user accomplishment'
  param :id, String, required: true
  def destroy
    destroyed = nil
    accomplishment = current_user.profile.accomplishments.find(params[:id])
    destroyed = accomplishment.destroy unless accomplishment.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:accomplishment).permit!
  end

end
