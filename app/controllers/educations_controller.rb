class EducationsController < ApplicationController

  api :GET, 'educations', 'current user experiences'
  def index
    render json: current_user.profile.educations
  end

  api :POST, 'experiences', 'create user experiences'
  param :education, Hash, :desc => 'create user education', required: true do
    param :school, String, required: true
    param :degree, String, required: true
    param :field_of_study, String, required: true
    param :activities, String, required: false
    param :from_year, String, required: true
    param :to_year, String, required: false
    param :description, String, required: false
    param :current, [true, false], required: false
    param :average_grade, String, required: false
  end
  def create
    render json: current_user.profile.educations.create!(valid_params)
  end

  api :PATCH, 'educations/:id', 'update user educations'
  param :id, String, required: true
  param :education, Hash, :desc => 'create user education', required: true do
    param :school, String, required: false
    param :degree, String, required: false
    param :field_of_study, String, required: false
    param :activities, String, required: false
    param :from_year, String, required: false
    param :to_year, String, required: false
    param :description, String, required: false
    param :current, [true, false], required: false
    param :average_grade, String, required: false
  end
  def update
    education = current_user.profile.educations.find(params[:id])
    education.update_attributes!(valid_params)
    render json: education
  end

  api :DELETE, 'educations/:id', 'delete user education'
  param :id, String, required: true
  def destroy
    destroyed = nil
    education = current_user.profile.educations.find(params[:id])
    destroyed = education.destroy unless education.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:education).permit!
  end

end
