class ProfilesController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'profiles', 'current user profile'
  param :handle, String, required: false, desc: 'User handle name'
  def index
    user = params[:handle].present? ? User.find_by_handle(params[:handle]) : current_user
    render json: user.user_with_profile_photo
  end

  api :POST, 'profiles', 'create current user profile'
  param :profile, Hash, :desc => 'create user profile', required: true do
    param :overview, String, required: true
    param :title, String, required: true
    param :birthday, String, required: true
    param :address, String, required: true
    param :photo, File, required: false
  end
  def create
    params[:profile][:user_id] = current_user.id
    profile = Profile.create!(valid_params)
    render json: profile
  end

  api :PATCH, 'profiles/:id', 'update current user profile'
  param :id, String, required: true
  param :profile, Hash, required: true do
    param :overview, String, required: false
    param :title, String, required: false
    param :birthday, String, required: false
    param :address, String, required: false
    # param :photo, File, required: false
  end
  def update
    profile = current_user.profile
    profile.update_attributes!(valid_params)
    render json: current_user.user_with_profile_photo
  end

  private

  def valid_params
    params.require(:profile).permit(:overview, :title, :birthday, :address, :user_id)
  end

end
