class ExperiencesController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'experiences', 'current user experiences'
  def index
    user = params[:handle].present? ? User.find_by_handle(params[:handle]) : current_user
    render json: user.profile.try(:experiences)
  end

  api :POST, 'experiences', 'create user experiences'
  param :experience, Hash, :desc => 'create user experience', required: true do
    param :title, String, required: true
    param :company, String, required: true
    param :location, String, required: true
    param :from_month, Integer, required: true
    param :from_year, Integer, required: true
    param :to_month, Integer, required: false
    param :to_year, Integer, required: false
    param :description, String, required: false
    param :current, [true, false], required: false
  end
  def create
    render json: current_user.profile.experiences.create!(valid_params)
  end

  api :PATCH, 'experiences/:id', 'update user experiences'
  param :id, String, required: true
  param :experience, Hash, :desc => 'create user experience', required: true do
    param :title, String, required: false
    param :company, String, required: false
    param :location, String, required: false
    param :from_month, Integer, required: false
    param :from_year, Integer, required: false
    param :to_month, Integer, required: false
    param :to_year, Integer, required: false
    param :description, String, required: false
    param :current, [true, false], required: false
  end
  def update
    experience = current_user.profile.experiences.find(params[:id])
    experience.update_attributes!(valid_params)
    render json: experience
  end

  api :DELETE, 'experiences/:id', 'delete user experience'
  param :id, String, required: true
  def destroy
    destroyed = nil
    experience = current_user.profile.experiences.find(params[:id])
    destroyed = experience.destroy unless experience.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:experience).permit!
  end

end
