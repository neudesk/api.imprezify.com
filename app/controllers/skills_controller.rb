class SkillsController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'skills', 'current user skills'
  def index
    render json: current_user.profile.skills
  end

  api :POST, 'skills', 'create user skills'
  param :skill, Hash, :desc => 'create user skills', required: true do
    param :name, String, required: true
    param :rating, String, required: true
  end
  def create
    skill = current_user.profile.skills.create!(valid_params)
    render json: skill
  end

  api :PATCH, 'skills/:id', 'update user skills'
  param :id, String, required: true
  param :skill, Hash, :desc => 'update user skills', required: true do
    param :name, String, required: false
    param :rating, String, required: false
  end
  def update
    skill = current_user.profile.skills.find(params[:id])
    skill.update_attributes!(valid_params) unless skill.blank?
    render json: skill
  end

  api :DELETE, 'skills/:id', 'delete user skill'
  param :id, String, required: true
  def destroy
    destroyed = nil
    skill = current_user.profile.skills.find(params[:id])
    destroyed = skill.destroy unless skill.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:skill).permit!
  end

end
