class UsersController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'users', 'get current user'
  param :handle, String, required: false, desc: 'User handle name'
  def index
    render json: params[:handle].present? ? User.find_by_handle(params[:handle]) : current_user
  end

  api :PATCH, 'users/:id', 'update user data'
  param :id, String, required: true, desc: 'any number'
  param :user, Hash, :desc => 'update user', required: true do
    param :name, String, required: false
  end
  def update
    current_user.update_attributes(valid_params)
    render json: current_user
  end

  private

  def valid_params
    params.require(:user).permit(:name, :handle)
  end


end
