class CharReferencesController < ApplicationController

  before_action :authenticate_user!

  api :GET, 'experiences', 'current user char_references'
  def index
    render json: current_user.profile.char_references
  end

  api :POST, 'char_references', 'create user char_references'
  param :char_reference, Hash, :desc => 'create user char_reference', required: true do
    param :name, String, required: true
    param :title, String, required: true
    param :contact_number, String, required: true
  end
  def create
    render json: current_user.profile.char_references.create!(valid_params)
  end

  api :PATCH, 'char_references/:id', 'update user char_references'
  param :id, String, required: true
  param :char_reference, Hash, :desc => 'create user char_reference', required: true do
    param :name, String, required: false
    param :title, String, required: false
    param :contact_number, String, required: false
  end
  def update
    char_reference = current_user.profile.char_references.find(params[:id])
    char_reference.update_attributes!(valid_params)
    render json: char_reference
  end

  api :DELETE, 'char_references/:id', 'delete user char_reference'
  param :id, String, required: true
  def destroy
    destroyed = nil
    char_reference = current_user.profile.char_references.find(params[:id])
    destroyed = char_reference.destroy unless char_reference.blank?
    render json: { success: destroyed ? true : false }
  end

  private

  def valid_params
    params.require(:char_reference).permit!
  end

end
