class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable,
         :validatable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_one :profile

  before_create :generate_default_handle


  def user_with_profile_photo
    json_profile = self.profile.as_json
    unless self.profile.nil?
      json_profile['profile_photos'] = {
          passport: API_BASE_URL + self.profile.photo.url(:passport),
          passport: API_BASE_URL + self.profile.photo.url(:thumb),
          large: API_BASE_URL + self.profile.photo.url(:large),
      }
    end
    json_profile
  end

  def admin?
    role == 4
  end

  def support?
    role == 3
  end

  def company_admin?
    role == 2
  end

  def individual?
    role == 1
  end

  private

  def generate_default_handle
    self.handle = BCrypt::Password.create("#{Faker::Superhero.name} #{Time.now}")
  end

end
