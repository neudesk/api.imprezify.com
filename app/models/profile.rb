class Profile < ApplicationRecord

  belongs_to :user
  has_many :languages
  has_many :contacts
  has_many :skills
  has_many :educations
  has_many :experiences
  has_many :accomplishments
  has_many :char_references

  validates_presence_of :title
  validates_presence_of :overview
  validates :overview, :title, :address, :birthday, presence: true

  has_attached_file :photo, styles: { passport: "133x170>", thumb: "94x132>", large: "265x340>" }, default_url: "http://via.placeholder.com/133x170"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/, size: { in: 0..10.kilobytes }

end
