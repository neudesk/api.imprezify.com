class Education < ApplicationRecord

  belongs_to :profile
  validates :school, :degree, :field_of_study, :from_year, presence: true
  validates :to_year, presence: true, :if => :current_is_false?

  private

  def current_is_false?
    current == false
  end

end
