class CharReference < ApplicationRecord

  belongs_to :profile
  validates :name, :title, :contact_number, presence: true

end
