class Contact < ApplicationRecord

  belongs_to :profile

  LIST = [{name: 'email', icon: 'fa-envelope'},
          {name: 'skype', icon: 'fa-skype'},
          {name: 'mobile', icon: 'fa-mobile'},
          {name: 'phone', icon: 'fa-phone'},
          {name: 'linkedin', icon: 'fa-linkedin'},
          {name: 'facebook', icon: 'fa-facebook'},
          {name: 'website', icon: 'fa-chrome'}]

  validates :name, :value, presence: true
  validates_inclusion_of :name, in: LIST.map {|x| x[:name]}, message: 'is not included in the list', if: :name_is_present?
  before_save :setIcon

  private

  def name_is_present?
    name.present?
  end

  def setIcon
    ico = LIST.select {|x| x[:name] == self.name}.first
    self.icon = "fa #{ico[:icon]}" unless ico.blank?
  end

end
