class SkillCategory < ApplicationRecord
  has_many :skill_tags
  validates :name, presence: true
end
