class Accomplishment < ApplicationRecord

  belongs_to :profile

  validates :accomplishment_type, :title, :description, :from_month, :from_year, :to_month, :to_year, presence: true

end
