class Skill < ApplicationRecord

  belongs_to :profile
  validates :name, :rating, presence: true
  validate :rating_value, if: :rating_is_present?

  private

  def rating_is_present?
    rating.present?
  end

  def rating_value
    errors.add(:rating, 'should be greater than 0 and lesser than 5') unless (rating.to_f >= 0 && rating.to_f <= 5)
  end

end
