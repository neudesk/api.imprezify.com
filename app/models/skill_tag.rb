class SkillTag < ApplicationRecord
  belongs_to :skill_categories
  validates :name, presence: true
end
