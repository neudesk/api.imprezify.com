class Experience < ApplicationRecord

  belongs_to :profile

  validates :title, :company, :location, :from_month, :from_year, presence: true
  validates_inclusion_of :from_month, :in => 1..12
  validates_inclusion_of :from_year, :in => (Time.now - 60.years).year..Time.now.year
  validates_inclusion_of :to_month, :in => 1..12, :if => :to_month_present?
  validates_inclusion_of :to_year, :in => (Time.now - 60.years).year..Time.now.year, :if => :to_year_present?

  private

  def to_month_present?
    to_month.present?
  end

  def to_year_present?
    to_year.present?
  end

end
