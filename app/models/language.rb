class Language < ApplicationRecord

  belongs_to :profile

  validates :name, presence: true
  validates_inclusion_of :name, in: Dictionaries::Languages.map {|x| x[:name]}

end
